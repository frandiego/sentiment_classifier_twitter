# Sentiment Classifier Twitter

In this repository you will find the python code to:
 1. download a dataset of more than 1.6 million tweets and to calculate the intensity of the feeling (polarity) of each one of them as well as its classification of the feeling in positive, negative or neutral (the code is in the `etl` folder, `etl/download.py` and `etl/sentiment.py`)
 2. create a ML model to classifiy a tweet as positive, negative or neutral  `sentiment_classifier.ipynb`
***
## Dependencies
* Python 3.8.5
* Pipenv [github](https://github.com/pypa/pipenv) and [docu](https://pipenv-fork.readthedocs.io/en/latest/)
    * Pipenv is a tool that aims to bring the best of all packaging worlds (bundler, composer, npm, cargo, yarn, etc.) to the Python world. Windows is a first-class citizen, in our world.
***
## Executions
To make the execution easier I have created a Makefile that allows to:
1. create the virtual environment and the Pipfile for the execution of the notebook and the execution of etl (you don't need to execute it because the Pipfile and Pipfile.lock are created)
```sh
# etl pipefile
$ make _install_etl_pipfile
# notebook pipefile
$ make _install_notebook_pipfile
```

2. Download the dataset and pre-process it to calculate the sentiment polarity and label of each tweet. 
```sh
# download
$ make download
# calculate polarity and label
$ make sentiment
# both download and calculate polarity and label
$ make etl
```

3. Running the notebook to learn how the classification model is made
```sh
# run jupyter notebook
$ make notebook
```

