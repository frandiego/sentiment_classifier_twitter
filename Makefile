.PHONY: help


ENVIRONMENT := local
PATH_ETL := etl
CONFIG := ./$(PATH_ETL)/config.ini
NOTEBOOK := sentiment_classifier.ipynb

help:
	@echo "Please use 'make <target> [params]' where <target> is one of"
	@echo "  download               Download sentiment140 dataset"
	@echo "  sentiment              Calculate sentiments using VADER"
	@echo "  etl                    download and sentiment"
	@echo " "

_install_pipfile:
	pipenv --python 3.8.5 install -r requirements.txt

_install_etl_pipfile:
	PIPENV_VENV_IN_PROJECT=$(PATH_ETL)
	pipenv --python 3.8.5 install -r $(PATH_ETL)/requirements.txt


download:
	PIPENV_PIPFILE=$(PATH_ETL)/Pipfile \
	pipenv run python $(PATH_ETL)/download.py --env=$(ENVIRONMENT) --config=$(CONFIG)

sentiment:
	PIPENV_PIPFILE=$(PATH_ETL)/Pipfile \
	pipenv run python $(PATH_ETL)/sentiment.py --env=$(ENVIRONMENT) --config=$(CONFIG)

etl: download sentiment


notebook:
	PIPENV_PIPFILE=Pipfile \
	pipenv run jupyter notebook $(NOTEBOOK)