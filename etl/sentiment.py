from vaderSentiment.vaderSentiment import SentimentIntensityAnalyzer
from tools import parse_argv
import pandas as pd

import configparser
import numpy as np
import logging

import sys
import os

# read args and config
arg = parse_argv(sys.argv)
config = configparser.ConfigParser()
config.read(arg.config)

# VADER Valence Aware Dictionary and sEntiment Reasoner, is a lexicon rule-based and sentiment analysis tool
# tool that is specifically attuned to sentiments expressed in social media.
# followings functions allow us to transform faster text in polarity using VADER
vader_scorer = SentimentIntensityAnalyzer().polarity_scores
polarity_scorer = lambda x: vader_scorer(str(x)).get('compound')
polarity_scorer_vec = np.vectorize(polarity_scorer)


# VADER returns the polarity (compound), this function classify this polarity in negative, positive and neutral
def compound_to_sentiment(x, alpha=0.05):
    # as indicated here https://github.com/cjhutto/vaderSentiment
    if x <= -alpha:
        return 'negative'
    elif x >= alpha:
        return 'positive'
    else:
        return 'neutral'


compound_to_sentiment_vec = np.vectorize(compound_to_sentiment)

# sentiment sensitivity (alpha of previous function) used to classify polarity
sentiment_sensitivity = float(config.get(arg.env, 'sentiment_sensitivity'))

if __name__ == "__main__":
    logging.getLogger().setLevel(logging.INFO)

    # output filename
    output_filename = os.path.join(*map(lambda i: config.get(arg.env, i), ['data_path', 'file_name_output']))
    if os.path.exists(output_filename):
        logging.info(f'dataset {output_filename} has been already created.')
    else:

        # read the data
        filename = os.path.join(*map(lambda i: config.get(arg.env, i), ['data_path', 'file_name']))
        df = pd.read_csv(filename)

        # calculate polarity
        df['vader_polarity'] = df['text'].apply(polarity_scorer_vec)

        # classify in sentiment (positive, neutral, negative)
        df['vader_sentiment'] = compound_to_sentiment_vec(df['vader_polarity'], sentiment_sensitivity)

        # save
        df.to_csv(output_filename, index=False)
        logging.info(f'polarity and sentiments calculated in  {output_filename}')
