from nlp import load_dataset
from tools import parse_argv

import configparser
import pandas as pd
import logging

import sys
import os

# read args and config
arg = parse_argv(sys.argv)
config = configparser.ConfigParser()
config.read(arg.config)

if __name__ == "__main__":

    logging.getLogger().setLevel(logging.INFO)

    # FILENAME OF THE DATA
    filename = os.path.join(*map(lambda i: config.get(arg.env, i), ['data_path', 'file_name']))

    if os.path.exists(filename):
        logging.info(f'dataset {filename} has been already created')
    else:

        # DOWNLOAD DATASET AND SAVE IN MEMORY USING PANDAS
        dataset = load_dataset(config.get(arg.env, 'nlp_dataset_name'))
        df = pd.concat((dataset[i]._data.to_pandas() for i in ["train", "test"])).reset_index().drop_duplicates()

        # SAVE DATASET IN A CSV
        df.to_csv(filename, index=False)
        logging.info(f'dataset downloaded at {filename}')
