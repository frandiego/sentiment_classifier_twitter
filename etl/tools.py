from types import SimpleNamespace


def parse_argv(argv):
    m_args = map(lambda i:i.split('='),filter(lambda i:'=' in i, argv)) # extract arguments
    d_args = {i[0].replace('-',''):i[1] for i in m_args} # arguments in a list
    return SimpleNamespace(**d_args) # arguments namesapace